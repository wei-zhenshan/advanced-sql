--（1）假设有部门表和员工表，在添加员工的时候，该员工的部门编号如果在部门表中找不到，则自动添加部门信息，部门名称为"新部门"。

```sql
go
create trigger tri_insertssss
on people after insert
as
begin  
	 if (select DepartmentId from inserted) not in( select DepartmentId from Department) 
	 begin
	    insert into Department values('009','学校') 
	 end
end  

insert into People values('009','吕布','女',1000)

select * from Department
select * from People
```

--（2）触发器实现，删除一个部门的时候将部门下所有员工全部删除。

```sql
go
create trigger tri_deletes
on department after delete
as
begin
	delete from People where DepartmentId=(select DepartmentId from deleted)
end

delete from Department where DepartmentName='市场部'

select * from Department
select * from People
```

--（3）创建一个触发器，删除一个部门的时候判断该部门下是否有员工，有则不删除，没有则删除。

```sql
go
create trigger tri_DeleteDeparyment
on department after delete
as
begin
    if (select DepartmentId from deleted) in  (select DepartmentId from Department) 
	  begin
	     print'删除失败'
	  end
    else
      begin
	     delete from People where DepartmentId=(select DepartmentId from deleted )	     
	  end
end

drop trigger tri_DeleteDeparyment

delete from Department where DepartmentName='市场部'
```

--（4）修改一个部门编号之后，将该部门下所有员工的部门编号同步进行修改

```sql
go
create trigger tri_update
on department after update
as
begin
   update People set DepartmentId =(select DepartmentId from inserted) where DepartmentId=(select DepartmentId from deleted)
end 

update Department set DepartmentId ='110' where DepartmentId='001'

select * from Department
select * from People
```