--（1）编写一个函数求该银行的金额总和

```sql
go
create function moneysum()
returns table
as
	return select sum(CardMoney) 金额总和 from BankCard
go

select * from moneysum()
```

--（2）传入账户编号，返回账户真实姓名

```sql
go
create function zname(@id int)
returns varchar(20)
as
begin
	declare @name varchar(20)
	select @name=RealName from AccountInfo where AccountId=@id
return @name
end
go
select dbo.zname(2)
```

--（3）传递开始时间和结束时间，返回交易记录（存钱取钱），交易记录中包含 真实姓名，卡号，存钱金额，取钱金额，交易时间。

```sql
go
create function cards(@begindate varchar(20),@enddate varchar(20))
returns table
as

return select RealName,b.CardNo,MoneyInBank,MoneyOutBank,ExchangeTime from CardExchange c
join BankCard b on c.CardNo=b.CardNo
join AccountInfo a on b.AccountId=a.AccountId
where ExchangeTime > '2022-09-01 00:00:00'  and ExchangeTime < '2022-09-30 23:59:59'
go

select * from cards('2022-09-01','2022-09-30')

drop function cards
```