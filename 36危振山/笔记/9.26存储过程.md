##### 自定义存储过程

**创建存储过程**

1.没有输入参数，没有输出参数的存储过程。

```sql
create proc <存储过程名称>
as
<t-sql语句>
go
```

2.有输入参数，没有输出参数的存储过程

```sql
create proc <存储过程名称>
<变量1> <数据类型>
<变量2> <数据类型>
...
as
<t-sql语句>
go
```

3.有输入参数，没有输出参数，但是有返回值的存储过程（返回值必须整数）。

```sql
create proc <存储过程名称>
<变量1> <数据类型>
<变量2> <数据类型>
...
as
<t-sql语句>
return 整数
go
```

4.有输入参数，有输出参数的存储过程 &  一个变量具备同时输入输出参数的存储过程

```sql
create proc <存储过程名称>
<变量1> <数据类型> output
<变量2> <数据类型> output
...
as
<t-sql语句>
return 整数
go
```

**执行存储过程**

```sql
--无参
exec <存储过程名称>
--带参
exec <存储过程名称> <形参1>,<形参2>,...
--带参带返回值
declare @变量
exec @变量 = <存储过程名称> <形参1>,<形参2>,...
--有输入参数，有输出参数的存储过程
declare @变量
exec <存储过程名称> <形参1>,<形参2>,@变量 output
--一个变量同时具备输入输出功能
declare @变量 <数据类型> = 值
exec <存储过程名称> <形参1>,<形参2>,@变量 output
```

**删除存储过程**

```sql
drop procedure <存储过程名称>
```

### 基本语法

#### 1.变量声明

　　　declare @variable int  或者 @variable int 

　　　多个变量的声明 declare @variable int, @va vachar(10),.... 　　　

　　　区别：declare的含义是定义一个存储过程中使用的变量，而不加declare的是存储过程需要传入的参数，下面一个具体的实例可以看得更清楚：

```sql
--创建存储过程
create procedure sl_procedure
@va int  --参数声明
as 
declare @variable int    --变量声明
set @variable=22 --变量赋值
select * from Albums
where ArtistId=@va

--执行存储过程
exec sl_procedure 12  --12 为传入的参数
```

#### 2.变量赋值：　　

```sql
set @variable=22 --变量赋值
```

　　 变量赋值时变量前必须加set

　　3.条件控制语句：

```sql
if(条件)
    begin
    --执行语句块
    end
else
    begin
    --执行语句块
    end
```

#### 3.循环控制语句

```sql
while(条件)
    begin
    --执行语句块
    end
```